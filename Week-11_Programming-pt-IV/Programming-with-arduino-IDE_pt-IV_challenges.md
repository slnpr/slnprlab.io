### Programming with Arduino part IV _ Challenges


1. Write a recursive function mult(a, b) which returns the multiplication of two numbers. (Hint:
Multiplying 4x3 is equivalent to adding 4+4+4.)

2. Write a function that takes a positive integer K and checks if K is a triangular number and
returns 1 (for TRUE) or 0 (for FALSE). A triangular number is a number obtained by the continued
summation of natural numbers. For example, 1, 1+2, 1+2+3, 1+2+3+4, etc., corresponding to 1, 3, 6,
10, etc., are triangular numbers.

3. Create a Bubble Sorting function that takes an array of numbers and sorts the array in ascending
order (smallest to biggest).
Example:
~~~
 int array[3][3] = {
 {1, 0, 0},
 {0, 1, 0},
 {0, 0, 1},
 };
 reverseImage(array);
~~~
Hints: Bubble Sort compares the i and i+1 indices of an array like so:
~~~
if(unsortedData[i] > unsortedData[i+1])
~~~
Here is how you create a function which accepts an array of integers:
~~~
void SortNumsAscending(int array[], int arrayLength){
//function goes here
}
~~~

4. Suppose an image can be represented as a 2D array of 0s and 1s. Write a function to reverse an
image. Replace the 0s with 1s and vice versa.

Example:
~~~
reverseImage([
 [1, 0, 0],
 [0, 1, 0],
 [0, 0, 1]
])
 [
 [0, 1, 1],
 [1, 0, 1],
 [1, 1, 0]
]
~~~


#### Challenge 1 


![](TooMuchMultiplication.png)
![](multiplication.png)

~~~
void setup() {
  Serial.begin(9600);

  Serial.println(Mult(4,3));
}

void loop() {
  // put your main code here, to run repeatedly:

}

int Mult(int a, int b) {
  if (b==0){
    return 0;
  }
  else {
    return Mult(a, b-1)+a;
  }
}

~~~

#### Answer

~~~
int mult(int a, int b){
if(a==0)
return 0;
else
return b + mult(a-1,b);
}
~~~

#### Challenge 2


![](TriangularNumberPos.png)

- -5 is not positive

![](TriangularNumberNot.png)

- 5 is not a triangular number

![](TriangularNumberYes.png)

- While triangular number is recognized, serial monitor returning 0

~~~
void setup() {
Serial.begin(9600);

Serial.println(TriangularNum(6));
}

void loop() {
  // put your main code here, to run repeatedly:

}

int TriangularNum(int k) {
  if (k < 0) { 
    Serial.println("This number is not positive");
    return 0;
  }
  int a = 1;
  TriangularNumRec(k,a);
}
  
  int TriangularNumRec(int k, int a) {
    if (k < a && k > 0) {
      Serial.println("This number is not triangular");
      return 0;
    }
    else if (k == 0) {
      Serial.println("This number is triangular");
      return 1;
    }
    else {
      TriangularNumRec(k-a, a+1);
    }
  }

~~~

#### Answer

~~~
void setup()
{
Serial.begin(9600);
int result = TriangleTest(3);
Serial.println(result);
}
void loop()
{
}
int TriangleTest(int K){
int sum = 0;
for (int i = 0; i < (K+1); i++){
sum = sum + i;
if (K == sum){
return 1;
}
else if (K < sum){
return 0;
}
}
}
~~~


#### Challenge 3

~~~

~~~


#### Challenge 


"exit status 1
expected identifier before '[' token" 

Error that I don't know how to solve

![](ReverseImageError.png)

~~~
void setup() {
  Serial.begin(9600);
  int a[3][3] = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
  Serial.println(ReverseImage);

}

void loop() {
  // put your main code here, to run repeatedly:

}

int ReverseImage([[1, 0, 0], [0, 1, 0], [0, 0, 1]]){
  for(int i = 0; i < 3; i++){ //line
    for(int j = 0; j < 3; j++){ //column
      if([i][j]==0){ //if equal to 0 change to 1
        [i][j]=1;
      }
      else { //if equal to 1 change to 0
        [i][j] = 0;
      }
    }
  }
}
  
~~~


#### Correction

~~~
int array[3][3] = {
{1, 0, 0},
{0, 1, 0},
{0, 0, 1},
};
reverseImage(array);
~~~

~~~
void reverseImage(int array[3][3]) {
}
~~~
