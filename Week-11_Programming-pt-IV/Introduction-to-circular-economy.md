### Introduction to Circular Economy

Episode 1 : Which Circular Economy ? Visions & Misconceptions

Ellen Macarthur Foundation’s Webinar "From Linear to Circular : Open to All"

![](Week-11_Programming-pt-IV/Circular-economy.pdf)
https://gitlab.com/de-fablab-manager/pre-fabacademy-courses/-/blob/master/Week-11_Programming-pt-IV/Circular-economy.pdf


> Assignment realised with LibreOffice Impress (https://fr.libreoffice.org/)

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Licence Creative Commons Attribution 4.0 International</a>.