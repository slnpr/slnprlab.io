### Basic Programming part V - Challenge : Glitching/Datamoshing


#### Testing 3D Glitching on the Benchy


Using the Processing sketch given in the course :
~~~
// Open a file and read its binary data
byte b[] = loadBytes("/home/fablab/test.stl");
for (int i = 0; i < b.length; i++) {
 if (b[i] == 0) {
 b[i]= 127;
 }
}
// Writes the bytes to a file
saveBytes("/home/fablab/test127.stl", b);
~~~

![](Week-11_Programming-pt-IV/Benchy-3dglitch-test-1_processing.png)

![](Week-11_Programming-pt-IV/Benchy-3dglitch-test-1_global.png)


> All the glitch tested

![](Week-11_Programming-pt-IV/Benchy-3dglitch-tests.mp4)

#### Testing 3D Glitching on a Rook


> All the glitch tested, using the same sketch as for the Benchy

![](Week-11_Programming-pt-IV/Rook-3dglitch-tests.mp4)





### Glitching Program


![](Glitchtest5.png)
![](test5.png)

![](Glitchtest6.png)
![](test6.png)

![](Glitchtest7.png)
![](test7.png)

#### Finding the RGB values of the BMP file 

Thanks to this very useful article linked in the course : https://medium.com/sysf/bits-to-bitmaps-a-simple-walkthrough-of-bmp-image-format-765dc6857393


![](test9.png)
![](test10.png)
![](test11.png)


![](Glitchtest13.png)
![](test13.png)

![](test14.png)
![](test15.png)
![](test16.png)
![](test17.png)

![](Glitchtest20.png)
![](test20.png)

![](Glitchtest21.png)
![](test21.png)



#### Modified sketch
~~~
// Open a file and read its binary data
byte b[] = loadBytes("test.bmp");
int a = 0;
for (int i = 41; i < b.length - 3; i += 3) 
{
  byte red = b[i];
  byte green = b[i+1];
  byte blue = b[i+2];
  
  println("red " + red + " blue " + blue + " green " + green);

  
  b[i] = red;
  b[i+1] = green;
  b[i+2] = blue;
  
  
  //b[i] = byte(b[i]);
  a = i;
}
println("done " + a);
// Writes the bytes to a file
saveBytes("test25.bmp", b);
~~~

#### Original sketch

~~~
// Open a file and read its binary data
byte b[] = loadBytes("/home/fablab/test.stl");
for (int i = 0; i < b.length; i++) {
 if (b[i] == 0) {
 b[i]= 127;
 }
}
// Writes the bytes to a file
saveBytes("/home/fablab/test127.stl", b);
~~~