### My Machines (a.k.a. my cheap fablab)


#### Creality Ender 3 3d printer

![](ender3.png)

Specification :

Technical Specifications

Ender-3 V2 FDM 3D Printer Properties

Modeling Technology: FDM（Fused Deposition Modeling）

Printing Size: 220x220x250mm

Filament: PLA/TPU/PETG

Working Mode: Online or SD card offline

Supported OS: MAC/WindowsXP/7/8/10

Filament Diameter: 1.75mm

Slicing Software: Simplify3d/Cura

Ender-3 V2 FDM 3D Printer Hardware

Machine Size : 475x470x620mm

Product Weight: 7.8KG

Package Weight: 9.6KG

Power Supply: Input AC 115V/230V; Output DC 24V 270W

Ender-3 V2 FDM 3D Printer Extruder Hardware

Layer Thickness: 0.1-0.4mm

Print Precision: ±0.1mm

Hotbed Temperature: ≤100°

User manual : https://cdn-global-hk.hobbyking.com/media/file/e/n/ender_3_1.pdf

#### 3018 CNC Engraver

![](318cncengraver.png)

Specifications :

Modèle: Mini CNC3018 (Sans module laser)

Matériel de cadre: Aluminium

Alimentation: DC 24V 5.6A

Zone de travail: Approx. 300x180x40mm

Taille de la machine: Approx. 400x330x240mm

Logiciel de contrôle: GRBL

Moteur pas à pas: 42 moteurs pas à pas, tension: 12V, courant de phase 1.3A, couple 0.25Nm

Moteur de broche: 775 moteur de broche (12-36V) 24V: 7000r / min, 36V: 9000r / min

Système d'exploitation compatible: Windows XP, Win7, Win8, Win10

Poids net: 7.5KG

Caractéristiques:

- L'assemblage nécessite une certaine habileté mécanique. Tous les composants d'assemblage sont inclus.

- Matériaux de gravure: La machine peut sculpter du bois, du plastique, de l'acrylique, des PCB ou des matériaux similaires.

- Ne peut pas graver: métal dur (tel que le laiton, l'acier, le fer, etc.), le jade et d'autres articles de texture très durs.

User manual : http://s3.amazonaws.com/s3.image.smart/download/101-60-280/SainSmart_Genmitsu_CNC_Router_3018-User_Manual.pdf

#### Soldering Iron

![](solderingiron.png)

Specifications :
- 60W 
- Voltage : 220V 
- Temperatures : 200-450 ℃