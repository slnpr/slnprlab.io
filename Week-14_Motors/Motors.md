### Motors

#### 1. Electromagnetism

- Right hand rule
- Polarité

Experimentation using :

- Screw x1
- Wire
- Battery

#### 2. DC Motors

- Permanent magnets + electromagnets > rotation
- Use of gears to module motor's torque and speed
- "eg. 100:1/1000:1" (reduction in term of revolution - equivalence between revolution and torque) 
- Direction of rotation depending of the polarity

#### H-Bridge

#### Stepper-Motors

- + precision

#### DC Brushless

#### Servo Motors

- Composed of a DC Motors combined with a potentiometer and an integrated H Bridge
- Turn at precise angles
- Square Waves
