### How to document Open Source Hardware projects ?

>sources :

>https://www.linkedin.com/pulse/c%25C3%25B3mo-documentar-proyectos-de-hardware-abierto-parte-cesar-garcia-1c/
>https://www.linkedin.com/pulse/c%25C3%25B3mo-documentar-proyectos-de-hardware-abierto-parte-cesar-garcia/
>https://www.youtube.com/watch?v=mEyxqcKm-JM

#### What is Open Source Hardware (OSH) ?

>https://www.oshwa.org/definition/
>https://opensource.org/osd

Hardware that is available with open source licences which permit to users to access the documentation and files in order to make it, to modify it, or to improve it.

#### Open Licencing options before 2020

**Open Source Hardware Association (OSHWA)** is a non-profit organisation promoting the Open Hardware's use. 
Open Source Hardware (self) Certification elements :
- Hardware (functional elements of the product)
- Software (code, firmware, or software involved in the produt functional)
- Documentation (including design files, schematics and instructions)
- Branding (brand name, product names, logos and product design)

When the certification process is completed, it gives the project an identification number relative to the country it is registered in.

**FabML** is a proposal for an open standard, proposed following a meeting of Dutch fablabs in 2013, coming to the conclusion that it would be wise to adapt a standard on which everyone could agree and which would serve to document the projects conceived in fablabs (what are the materials used, time, difficulty, etc.), but this project was not completed.

>http://giplt.nl/universiteitamersfoort/data/pdf/FabML.pdf

**O Manual** is a standard for documenting the processes of electronic devices in order to repair them, initially used by the Ifixit comunity. It included in the IEEE 1874 standard for the documentation diagram for the repair of electronic equipment.
O manual provides a file, similar to a .zip, which contain the device's decription, steps, xml file, images, etc. It is commercially implemented on the Dozuki plarform, which was launched by Ifixit to document business-oriented projects, and so, there is no free implementations of this standard, which limits its utility.

**Docubricks** is a platform which allows to generate a functional documentation from different modules. It provides a choice of preselected licences such as the CERN Open Hardware Licence.

>http://www.docubricks.com/

**Libre Objet** is a collective formed by designers, hackers and artists questionning the open source industrial design. In 2015 they published "Diverted Derived Design" which is an open-guide providing open object basis, by explaining why and how to "open" a design process, which licences exists and how they work, in order to allow anyone to free his projects.
>https://books.libreobjet.org/open-guide/en/index.html

**Instructables** had a mobile application which allowed to document a project by taking pictures and labelling them, but this tool is no longer available. 

#### New options 

>ressources :

>https://arxiv.org/abs/2004.07143
>https://osegermany.gitlab.io/OHS/DIN_SPEC_3105-1.pdf
>https://osegermany.gitlab.io/OHS/DIN_SPEC_3105-2.pdf
>https://gitlab.com/OSEGermany/OHS
>https://medium.com/@journalopenhw/din-spec-3105-explained-2cce6134c207


**Standardisation of practices in Open Source Hardware** is a paper proposing a standard establishing the contours of "Open Source Hardware" and what it means in practice. DIN SPEC 3105 intend to make clear what documentation and information is required to label a hardware project as open source, and act as "a clear reference endorsed by an official standardisation body." To do so it is composed of two parts :

**DIN SPEC 3105-1 : Requirements for technical documentation** 
The first part of the standard is build upon the "Open Source Hardware Definition 1.0". and "defines precise licensing terms under which hardware devices and their documentation should be released to align with the ethos of the open source movement". What is improved is that it sets the minimal requirements in term of content so that the documentation is sufficient for anyone to use the project as required by the previous definition.

There is 5 requirements to be completed for technical documentation :

1. General

"The Documentation of a piece of OSH bears references of : 
- its authors;
- its licensing terms;
- a functional description of OSH, e.g. what functions it is supposed to deliver, what is the problem it solves, for whom, etc.;
- a mention of all applying Technology-specific Document Criteria;
- a name or working title given to the Piece of Hardware."


2. Documentation release

" A Documentation Release makes mention of :
- a release date;
- a release number unambiguously identifying a version of the documentation;
- a version of the piece of OSH to which the documentation release applies."


3. Access

" The Documentation is deemed as accessible when :
- it is published;
- in its original editable file format and,
- in an export file format that
     - is of well-establshed use in the corresponding field of technology
     - can be processed by software that is generally accessible to the recipients, and
     - contains no less information thant the editable file format.
- the means of downloading it via the Internet is well-publicized and neither involve any charge or any moderation potentially conflicting with the principles of non-discrimination against persons or groups and non-discrimination against fields of endeavour.
- the means of downloading it via the Internet is constantly active from the Release date and without interruption.


4. Valid Technology-specific documenation criteria

>https://gitlab.com/OSEGermany/oh-tsdc/-/blob/master/TsDC-explanatory.md

"Valid TsDC :
- clearly refer to and extends DIN SPEC 3105-1 without superseding it;
- provide technology-specific requirements for all phases of the Life Cycle;
- are released under a free/open license;
- appear in the list of approved TsDX in Annex A"

![TsDC example](Week-14_Motors/TDdcexample.JPG)

5. Lifecycle of Piece of Hardware

"The following activities are part of the Life cycle:
- Realisation, spanning from raw material extraction, production of semi-finished products, final assembly. Activities in this place are aimed at establishing the functionality of the piece of open source hardware (3.3).
- Operation and maintenance, includinf activities centeres on delivering or maintaining th functionnality of the piece of open source hardware (3.3).
- End-of-life, including reuse, refurbishment, reconditionning, recycling, disposal. Activities in this phase are aimed at making the physical components or a subset of functions available for other pieces of hardware (3.1)."

**DIN SPEC 3105-2 : Community-based assessment**

The second part of the standard defines a community-based assesment procedure, between self-certification and third-party assesment.

How it works : 

1. General requirements

The community-based assesment process is performed online, is publicly accessible without restrictions and must remain this way. 


2. Issue and challenge an attestation
2.1. Application

When the DIN SPEC 3105-1 is completed, its author has to apply his documentation to a conformity assesment body.
"The application includes :
- name and contact detal of the client;
- the permanent URL to the documentation realease;
- the application date."

Once done, it opens the community-based assesment process and the conformity assesment body may request reviews from individuals it has selected.


2.2. Reviews and decisions

"A review includes : 
- an unambiguous reference to the corresponding documentation release;
- a mention of the documents that have been reviewed;
- an unambiguous reference to the reviewer;
- textual comments from the reviewer justifying his/her decision
- the decision of the reviewer."

Following this step, the reviewer can 
1. Approve, when the documentation is compliant;
2. Approve subject to revision, when documentation needs to be clarified.

If the reviewer decided for the Approval subject to revision the client can :
1. submit a revised documentation, and the assessment process may begin again;
2. submit arguments "to correct an eventual misinterpretation of the reviewer."


3. Attestation

Once the documentation submitted has been approved by twice decisions in its integrality, an attestation is issued. The "attestation 
- makes mention of its state which is either valid or void;
- clearly identifies the conformity assessment body as its author;
- is accessible through a permanent URL;
- bears a release date;
- includes the permanent URL to the corresponding documentation release;
- contains all corresponding reviews and unresolved complaints, or includes permanent URLs to them".

The reviewing process is closed as soon as the attestation is issued, but it can be re-open if complaints highlight misalignments in the documentaion. In this case, the attestation is void, the reviewing process is reopened, the misalignment is clarified and approved twice, and then the attestation may regains its valid state.


**Open Know How** is a search engine aiming at filtering, cross-linking and finding hardware documentation based on the Open Know-how Manifest Specification 1.0.

>https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/src/branch/master/1
>https://openknowhow.org/


#### Conclusion

More than enforcing a standard, the DIN SPEC 3105 is meaning to be a tool to conceive an efficient standard adaptable to the various fields encompassed in the open hardware community, and a standard growing in maturity thanks to the past assessments processes.
Second, DIN SPEC 3105 is the first standard to be hosted by a national organisation under an open source license, allowing this standard to be accessed, copied and distributed. Plus, the community-based assessment process of this open source standard is transparent. " it contributes to the emergence of shared practices and the establishment of a visible and trustable identity. More, it does this in a way that aligns with the ethos of transparency and participation that is key to the open source movement."
Finally, it adapts the Four Essential Freedoms of Free Software to the hardware, which are 
1. the freedom to use the hardware for any purpose;
2. the freedom to study the hardware;
3. the freedom to redistribute copies;
4. the freedom to distribute copies of the modified version.
