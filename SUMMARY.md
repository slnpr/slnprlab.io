# Summary

* [Intro](README.md)


## Electronics Basics

* [Eagle PCB Introduction](Week-09_Eagle/Eagle-pcb-workshop.md)

* [Eagle PCB Introduction part II - Lasercut PCB ](Week-07-06_PCB-part-2_Molding/Eagle-pcb-workshop-part-2.md)

* [Modular Synthetizers using Falstad](Week-08_Modular-Synthetizers -workshop_3018-Engraving-machine/Modular-synthetizers.md)

* [Logisim Introduction](Week-04_Logisim-intro/Logisim-intro.md)


## Proto Hut

* [My Machines](0_My-machines/My-machines.md)

* [Getting Started with the Mini CNC 3018](Week-08_Modular-Synthetizers -workshop_3018-Engraving-machine/3018-Engraving-Machine_getting-sarted.md)

* [The Harmonograph : a drawing machine](Week-12_Programming-pt-II/Harmonograph.md)

* [Molding Sawdust](Week-07-06_PCB-part-2_Molding/Molding-sawdust-test-1.md)

* [Design for the Atari Punk Console](Week-08_Modular-Synthetizers -workshop_3018-Engraving-machine/APC-Design.md)


## Programming Basics

* [Programming with Arduino part I](Week-13_Sensors/Programming-with-arduino-IDE_Challenges.md)

* [Programming with Arduino part II](Week-12_Programming-pt-II/Programming-with-arduino-IDE_pt-II_challenges.md)

* [Programming with Arduino part IV](Week-11_Programming-pt-IV/Programming-with-arduino-IDE_pt-IV_challenges.md)

* [Programming with Arduino part V](Week-11_Programming-pt-IV/Basic-programming-part-V-challenge.md)

* [Programming with Arduino part VI](Week-10_Programming-pt-VI/Programming-with-arduino-IDE_pt-VI_challenges.md)

* [Programming with Arduino part VII : Jonah's Drum Machine](Week-10_Programming-pt-VI/Drum-Machine-commands.md)


## Circular Economy

* [Introduction to Circular Economy](Week-11_Programming-pt-IV/Introduction-to-circular-economy.md)


## Documentation about Open Source

* [Open Source Hardware Documentation](Week-14_Motors/Open-Source-Hardware-Documentation.md)

## Other

* [DICASO](Week-03-02-01_DICASO-Prototype/DICASO-Project.md)
