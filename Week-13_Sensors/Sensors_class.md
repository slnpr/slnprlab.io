### Sensors

#### 1. What is a sensor  ?

a. Active/passive

b. Analog/digital

c. Sensor/Actuator

#### 2. Signal conditioning

a. OP-amp :

- inverting op-amp
- non inverting op-amp

b. Filters

c. Mic amp

#### 3. Sensors & Arduino

a. Digital vs Analog reading

b. "Pull-up" resistors

c. Multiplexer

d. Analog to Digital Converter (ADC) & Digital to Analog Converter (DAC)

e. Resistance divider

f. Sensors & resistance divider

g. Infrared obstacle detection

h. sonar obstacle detection

i. Capacitive touch sensor

#### 4. Useful links

- Libraries : https://www.arduino.cc/en/Reference/Libraries

- Smoothing : https://www.arduino.cc/en/Tutorial/BuiltInExamples/Smoothing

average = total / numReadings;

- Calibrating : https://www.arduino.cc/en/Tutorial/BuiltInExamples/Calibration

sensorValue = map(sensorValue, sensorMin, sensorMax, 0, 255);

- I2C :  https://www.arduino.cc/en/Reference/Wire