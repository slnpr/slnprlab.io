### Programming with the Arduino IDE

#### 1. Variables

Think of the variable as a box, which keeps the same name, but its content may change. 

##### a. Variable data types

| data types | bytes | minimum | maximum |
| - | - | - | - |
| int (integer) | 2 | -32768 | 32767 |
| char (character) | 1 | -128 | 127 |
| byte | 1 | 0 | 255 |
| unsigned int | 2 | 0 | 65535 |
| long | 4 | -2147483648 | 2147483647 |
| unsigned long | 4 | 0 | 4294967295 |


##### b. Variable scope
~~~
int x = 0; // **global variable declaration**

void setup () {    
}
void loop () {
    int z = 0; // **Local variable declaration**
}
~~~

A variable initialized at the beginning of the program has a repercusion on all of it.

A variable initialized in the loop only aftereffects the loop it is in.


#### 2. Strings and Arrays


##### a. General

|  |  |  |  |  |  |  |
| - | - | - | - | - | - | - |
| 0 | 1 | 2 | 3 | 4 | 5 | 6 |

Arrays and strings are composed of several boxes which can contain distincts data types. Each of these boxes are indexed.

##### b. Strings

A string is composed of several boxes which usually contain ASCII characters. How to formulate it in the Arduino IDE :

~~~
void loop () {
    char my_str[6];

    my_str[0] = 'H';
    my_str[1] = 'E';
    my_str[2] = 'L';
    my_str[3] = 'L';
    my_str[4] = 'O';

    char like[] = "I like coffee and cake";
}
~~~

##### c. Arrays

| C | Content |
| - | - |
| C[0] | -45 |
| C[1] | 6 |
| C[2] | 0 |
| C[3] | 72 |
| C[4] | 1543 |
| C[5] | -89 |
| C[6] | 0 |
| C[7] | 62 |
| C[8] | -3 |
| C[9] | 1 |
| C[10] | 78 |


In this example, an array named C is composed of 11 elements indexed by numbers (from 0 to 10), each element contains disctinct values.
How it is formulate in a sketch :

~~~
type arrayName [ array size ] ;
int c[12]; // C is an array of 12 integers
~~~

#### 3. Control structures

##### a. if, if/else, switch/case

##### b. do, do/while, for

##### c. jump statements : continue, goto, break

+ flow diagrams


#### 4. Functions

Functions are displayed in orange inthe Arduinoe IDE.
All functions must have a unique name : **setup** and **loop**, which are part of the structure of a basic sketch, are functions in Arduino programming.
The function name is followed by parentheses **()**, and have a return type (which is **void* for setup and loop functions). Finally, the body of a function is enclosed in opening and closing braces **{}**.

How to write/define a function :

~~~
void DashedLine ()
{
    serial.println("----------------)
}
~~~

The components of the DashedLine() function :

![Components of the DashedLine Function](Week-13_Sensors/functions1.png)

A function as to be called as shown in the sketch below :

![DashedLine function example sketch](Week-13_Sensors/SketchDashedLine.JPG)

The DashedLine() function is created at the end of the sketch and called twice in the void setup.


#### 5. Operators


##### a. Arithmetic Operators ** + - * / % = **

~~~
void loop () {
    int a = 9, b = 4, c;

    c = a + b; //13
    c = a - b; //5
    c = a * b; //36
    c = a / b; //2
    c = a % b; //1
}
~~~

##### b. Comparison Operators ** == != < > <= >= **

~~~
void loop () {
    int a = 9, b = 4

    if(a == b){} //false
    if(a != b){} //true
    if(a < b){} //false
    if(a > b){} //true
    if(a <= b){} //false
    if(a >= b){} //true
}
~~~

##### c. Boolean Operators ** && || ! **

~~~
void loop () {
    int a = 9, b = 4

    if ((a > b) && (b < a)){} //true
    if ((a == b) || (b < a)){} //true
    if (!(a == b) && (b < a)){} //true
}
~~~

##### d. Bitwise Operators ** & | ^ ~ << >> **

~~~
void loop () {
    int a = 10; // 0b00001010
    int b = 20; // 0b00010100
    int c;

    c = a & b; //0b00000000
    c = a | b; //0b00011110
    c = a ^ b; //0b00011110
    c = -a ; //0b11110101
    c = a << b; //0b00101000
    c = a >> b; //0b00000101
}
~~~

##### e. Bitwise Operators ** ++ - += -= *= /= %= |= &= **

~~~
void loop () {
    int a = 10, b = 20;
    int c = 0;

    a++; //11
    a--; //9
    b += a; //30
    b -= a; //10
}
~~~