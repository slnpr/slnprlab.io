### Programming with Arduino IDE : Challenges

> Hello Selena,
>
> Here are some Arduino coding challenges for you. You can pick any one
> or more of the challenges. I'm available today to help troubleshoot,
> and we'll talk about your results on Monday at 11am.
>
>
> Challenges:
>
> 1. Take user input from the keyboard to change the color of an RGB LED.
>
> https://www.arduino.cc/en/Tutorial/BuiltInExamples/SwitchCase2
>
>
>
> 2. Create an array of 255 distinct values (bonus if the numbers are
> meaningful in some way).
>
> https://www.arduino.cc/reference/en/language/variables/data-types/array/
>
>
>
> 3. Tell if a user's input was even or odd.
>
> https://www.tutorialspoint.com/arduino/arduino_arithmetic_operators.htm
>
>
>
> 4. Take a diameter of a circle from the user and return the
> circumference as a decimal (e.g. 8.56930).
>
> https://www.arduino.cc/reference/en/language/variables/data-types/float/
>
>
>
> 5. BONUS: Make code that generates abstract ASCII art/drawings in the
> serial monitor!!
>
> https://en.wikipedia.org/wiki/ASCII_art
> https://manytools.org/hacker-tools/convert-images-to-ascii-art/
>
>
> Good luck!
>
> Jonah
>




#### 1. Take user input from the keyboard to change the color of an RGB LED. (switchCase2)



![LED RGB Circuit components](LEDRGBcircuitcomponents.png)

The circuit is composed of :

- 1 * Arduino Uno
- 1 * LED RGB
- 3 * 220 Ohms resistors
- 4 * jump wires

![LED RGB Circuit first attempt](LEDRGB1stattempt.png)

- note : the RGB LED is linked to the 5V pin (the mistake that partially caused the following bug as I thought it was a common cathode RGB LED)

- Question about the RGB LED : how to differentiate common anode or common cathode ?

![Common anode, common cathode](commonanode-cathode.jpg)

- Code part, based on the SwithCase2 example provided with the Arduino IDE :

![RGB LED first attempt : nothing's happening](RGBLED-nothinghappens.png)

- After compiling this sketch, nothing is happening in the breadboard area, but the rx led switching on and off as the board receives commands I type in the serial monitor.

- There is an error I don't undertand, so I go to the "First thing first option" : I set up the SwitchCase2 circuit with 3*red LED of 5mm and 3*220 Ohms resistors to apprehend what should happen when it is  working.

![SwitchCase2 LEDs are not switching off](switchcase2-bug-noledswitchingoff.png)

- I changed the original led pins to the pins 9, 10, and 11, and linked the ground pin on the Arduino board instead of the 5V pin. The sketch is partially working : LEDs are switching on, but are not switching off.

![SwitchCase2 LEDs are not switching off once switched on](ledrgb-noledoff.mp4)

- Instead of the "default" case, i added another case named "d" in order to switch off the LEDs :

![SwitchCase2 switching off resolved](switchcase2modifcase-d.png)

~~~
case 'd':
    digitalWrite(9, LOW);
    digitalWrite(10, Low);
    digitalWrite(11, LOW);
~~~

- After this, the LEDs were switching off while entering "d" on the serial monitor. 

![SwitchCase2 switching off resolved](ledrgb_ledoff.mp4)


- Then I gave the RGB LED another try :


![SwitchCase2 with RGB LED(finally)](ledrgb_ledrgb.mp4)

- The sketch is working (with the ground linked and not the voltage), but the red light of the RGB LED is way more weaker than the blue and the green lights; why ?

note for display improvement : some stability is needed, and the keyboard on which I am typing the "proof commands" is missing from the field of view. 



#### 2. Create an array of 255 distinct values (bonus if the numbers are meaningful in some way). (Array)



![Create an array of 255 distinct values](Array255.png)

~~~
int A[255];                            //create an array composed of 255 elements

void setup() {
  Serial.begin(9600);                  //initialize the serial monitor

  for( int i = 254; i >= 0; i--){      //initialize a variable "i" which the value decrements until it reaches 0.
    A[i] = i;                          //define an element of the array as "i"
  Serial.println(A[i]);                //display the array in the serial monitor
  }
}

void loop() {
}
~~~

- It ends with a zero instead of beginning at zero, isn't it meaningful ? 



#### 3. Tell if a user's input was even or odd. (Arduino arithmetic operators)



![Even number](evenoroddeven.png)
![Odd number](evenoroddodd.png)

~~~
int incomingByte = 0;              //Create a variable for the data input

void setup() {
  Serial.begin(9600);
}

void loop() {
  
  if (Serial.available() > 0) {   //read the sensor
    incomingByte = Serial.read();
  }

  if (incomingByte%2 == 0){       //if the number read is divisible by two
    Serial.println ("even");      //it is an even number
    }
  else {                          //otherwise
    Serial.println ("odd");       //the value remaining means it is an odd number
  }
}
~~~



#### 4. Take a diameter of a circle from the user and return the circumference as a decimal (e.g. 8.56930). (Float)



![Circonference calculator](CirclePerimeter.png)

~~~
float Pi = 3.14159/25;            //Create a variable Pi 
float d = 0.0;                    //Creat a variable "d" as diameter

void setup() {
  Serial.begin(9600);
}

void loop() {
  if (Serial.available() > 0.0) { 
  d = Serial.read();
  float c = Pi * d;               //Create "c", Circonference is equal to Pi x diameter
  Serial.println(c, 3);           //print the result on the monitor, with 3 digits after the comma.
  }
}
~~~

- note : To create Pi and obtain a right result, I had to divide Pi by 25, but I don't know why or what caused this. Is that normal ?
> The Serial.read() is receiving Ascii characters : In ASCII, 0 is equivalent to 48 in decimal value. 2 is equivalent to 50, etc. (more info here : http://www.asciitable.com/)
To solve this problem this line was added :

~~~
d = Serial.read() -48;
~~~

![debug with Serial.print()](circleperimeterdebug1.png)

![](circleperimeterdebug2.png)


#### 5. BONUS: Make code that generates abstract ASCII art/drawings in the serial monitor!!



![From middle, to right, to left : converting a .JPG file into ASCII characters](generateascii0.png)
![Error while compiling the sketch](asciigenerator-notcompiling.png)

- The error occuring : "Les variables globales utilisent 2132 octets (104%) de mémoire dynamique, ce qui laisse -84 octets pour les variables locales. Le maximum est de 2048 octets". 
- I removed  lines 29 and 30 to regain storage space. (maybe a barbaric way ?)

![Generating ASCII](ascii-generating-serialmonitor1.mp4)

- I used Screenpresso software to obtain the video screenshot. The sketch is working, but could probably work better.
