### Jonah's Drum Machine 

The Drum Machine is a program written by Jonah Marrs.

The assignment here was to add commands to an existing sound sequencer in the arduino serial monitor.
I tried to draw shapes using nested loops, these shapes are combining in the serial monitor, as shown in the videos below, creating new partterns of sound everytyme a command is sent.


![](Drum-machine-output.m4v)


![](Drum-Machine-Test-1.m4v) 


#### Commands added to the Drum Machine

The Sketches for each command are indexed below this table.

| Command | Shape | Command | Shape | Command | Shape |
| - | - | - | - | - | - |
| a | ![](a.PNG) | b | ![](b.PNG) | c | ![](c.PNG) |
| d | ![](d.PNG) | e | ![](e.PNG) | f | ![](f.PNG) |
| g | ![](g.PNG) | h | ![](h.PNG) | j | ![](j.PNG) |
| k | ![](k.PNG) | s | ![](s.PNG) |


#### Sketch for each command

- a
~~~
case 'a':
         
      for (int  i = 0; i < 10; i++)
     {  
       for (int  j = 0; j < 20; j++)
      {                          
         array[i][j] = '+';          
           
         
         //else {              
            //array[i][j] = '-';          
           // }    
      }
   }  
   break;

~~~

- b
~~~
case 'b':
         
      for (int  i = 0; i < 10; i++)
     {  
       for (int  j = i; j < 20; j++)
      {                          
          array[i][j] = '+';          
           
         
        //else {              
            //array[i][j] = '-';          
           // }   
      }
   }  
   break;

~~~

- c
~~~
case 'c':
         
      for (int  i = 2; i < 7; i++)
     {  
       for (int  j = i; j < 17; j++)
       {                                  
        array[i][j] = '+';  
         
         if (i%2 == 0 && j%3 == 0) 
         {              
			array[i][j] = '-';           
            }      
        }
   }  
   break;

~~~

- d
~~~
case 'd':
         
      for (int  i = 0; i < 10; i++)
     {  
       for (int  j = i; j < 20; j++)
       {                          
        array[i][j] = '+';         
           if (i%2 == 0 && j%3 == 0) 
           {              
		  	array[i][j] = '-';           
            }   
        }
   }  
   break;

~~~

- e
~~~
case 'e':
         
       for (int  i = 2; i < 7; i++)
      {  
         for (int  j = i; j < 17; j++)
        {                                  
          array[i][j] = '+';    
          
        //else {              
            //array[i][j] = '-';          
           // }   
          }
   }  
   break;

~~~

- f
~~~
case 'f':
         
       for (int  i = 1; i < 8; i++)
       {  
         for (int  j = 7; j >= i; j--)
         {                          
           array[i][j] = '+';          
           }
    }  
   break;

~~~

- g
~~~
case 'g':
         
       for (int  i = 0; i <= 8; i++)
      {  
         for (int  j = 0; j < i; j++)
        {                          
           array[i][j] = '+';          
           }

    }  
   break;

~~~

- h
~~~
case 'h':
         
       for (int  i = 1; i <= 8; i++)
      {  
         for (int  j = 0; j <=(2 * i - 1); j++)
        {                          
           array[i][j] = '+';          
           }

    }  
   break;

~~~

- j
~~~
case 'j':
         
       for (int  i = 1; i <= 8; i++)
      {  
         for (int  j = 0; j <=(2 * i - 1); j++)
        {                          
           array[i][j] = '+';
           
           if (i%3 == 0 && j%3 == 0) 
          {              
		    	array[i][j] = '-';           
                }          
           }

    }  
   break;

~~~

- k
~~~
case 'k':
         
      for (int  i = 0; i < 10; i++)
     {  
       for (int  j = 0; j < 20; j++)
      {                          
          array[i][j] = '-';          

      }
   }  
   break;
~~~

- s
~~~
case 's':
         
      for (int  i = 3; i < 7; i++)
     {  
       for (int  j = 4; j < 13; j++)
      {                          
        array[i][j] = '+';          

         //else {              
            //array[i][j] = '-';          
           // }  
      }
   }  
   break;

~~~



#### The Drum Machine Code

~~~
/*

soundBox

A visual and interactive sound sequencer in the Arduino Serial Monitor

*/

char array [10][20]; // our screen
  
void setup()
{
  Serial.begin(115200); // fastest possible Serial Speed in order to "render" our screen quickly
  pinMode(8, OUTPUT); // piezo pin
  
  Serial.println();
  Serial.println();    
  Serial.println();
  Serial.println();
  Serial.println();
  Serial.println();
  Serial.println();
  Serial.println(" ~ Welcome to soundBox ~"); 
  Serial.println("available commands: i, r, t "); // Serial print list of available commands for user
  Serial.println();
  Serial.println();
  Serial.println();
  
  delay(3000);
  
  int rows = 10;
  int columns = 20;
  

  //FILL THE ARRAY WITH DASHES
  
       for (int  i = 0; i < rows; i++) 
   {  

       for (int  j = 0; j < columns; j++)
   {     

         array[i][j] = '-';      
         Serial.print(array[i][j]);
         
   	   }
         
     Serial.println();
   }

      Serial.println();
      Serial.println();
      refresh(array);
      print(array);
   }
  
  
void loop()
{
      listen(array);
      Serial.println();
      Serial.println();
      refresh(array);
      print(array);
        
  //indices(array);
}

void refresh(char array[10][20]){

int columns = 20;
int rows = 10;  
int xCounter = 0; 
  

//CHECKS TO SEE IF WE NEED TO PLAY ANY SOUNDS, I.E. IF ANY '+' IN THE COLUMN TO THE RIGHT
		for(int i=0;i<rows;i++){
          
          if(array[i][19] == '+')
          {
           xCounter++;  //increment x counter      
          }     
		}
  
  //BASED ON HOW MANY '+'S WE FOUND IN THE FINAL LINE, PLAY DIFFERENT SOUNDS
  
  //MAKE THIS PLAY A SAMPLE INSTEAD OF NOTES?
  
  switch (xCounter) {
  case 1:
    //do something when var equals 1
    tone(8, 100, 100);
    delay(100);
    break;
  case 2:
    //do something when var equals 2
    tone(8, 200, 100);
    delay(100);
    break;
  case 3:
    //do something when var equals 3
    tone(8, 300, 100);
    delay(100);
    break;     
  case 4:
    //do something when var equals 4
    tone(8, 400 , 100);
    delay(100);
    break;
  case 5:
    //do something when var equals 5
    tone(8, 500, 100);
    delay(100);
    break;
  case 6:
    //do something when var equals 6
    tone(8, 600, 100);
    delay(100);
    break; 
  case 7:
    //do something when var equals 7
    tone(8, 700, 100);
    delay(100);
    break;
  case 8:
    //do something when var equals 8
    tone(8, 800, 100);
    delay(100);
    break;     
  case 9:
    //do something when var equals 9
    tone(8, 900, 100);
    delay(100);
    break;
  case 10:
    //do something when var equals 10
    tone(8, 1000, 100);
    delay(100);
    break;
  default:
    // if nothing else matches, do the default
    noTone(8);
    delay(100);
    break;
}
  
xCounter = 0; // reset the counter
 
    /*
  
 //LOOPS THE LINES AROUND SHIFTING TO THE LEFT
for(int j=0;j<rows;j++){ 
	int temp = array[j][0] ;
		for(int i=0;i<columns;i++){
			array[j][i]=array[j][i+1];
		}
	array[j][19] = temp ;
	}
}

*/

  

 //LOOPS THE LINES AROUND SHIFTING TO THE RIGHT
  
for(int j=0;j<rows;j++){ 
	int temp = array[j][19];
		for(int i=19;i>0;i--){
			array[j][i]=array[j][i-1];
		}
	array[j][0] = temp ;
	}
}

  
void listen(char array[10][20]){
  
int columns = 20;
int rows = 10;  
  
int incomingByte = 0; // for incoming serial data

  if (Serial.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial.read();
  }
  
    //Here are the functions that modify the array:
  
  switch (incomingByte) {
  case 'r':
    
    //print a pattern when var equals 'r'

       for (int  i = 0; i < rows; i++) 
   {  
       for (int  j = 0; j < columns; j++) 
      {     
            array[i][j] = '+';         
         if (i%2 == 0 && j%3 == 0) 
         {              
			array[i][j] = '-';           
         }         
   	  }
   }
    break;
    
  case 'i':
    
    //invert the array when var equals i
    
    
    for (int  i = 0; i < rows; i++) 
   {  
       for (int  j = 0; j < columns; j++) 
        {              
         if (array[i][j] == '+') {              
			array[i][j] = '-';           
            }   
         
         else{             
			array[i][j] = '+';           
             }  
   	    }
   }     
    break;
    
    //draw a big '+' when var equals t 
    //available commands: a, b, c, d, e, f, g, h, i, j, r, t, s
  case 't':
          
       for (int  i = 0; i < rows; i++) 
   {  
       for (int  j = 0; j < columns; j++) 
       {            
         if (i == 4 || j==10) {              
			array[i][j] = '+';          
            }   
         
         else {              
			array[i][j] = '-';           
            }  
   	   }
   }   
   break; 
    
    
     //draw a square when var equals s
    //available commands: a, b, c, d, e, f, g, h, i, j, r, t, s
  case 's':
         
      for (int  i = 3; i < 7; i++)
     {  
       for (int  j = 4; j < 13; j++)
      {                          
        array[i][j] = '+';          
           
         
         //else {              
            //array[i][j] = '-';          
           // }  
      }
   }  
   break;
    
    //draw a big square when var equals a
    //available commands: a, b, c, d, e, f, g, h, i, j, r, t, s
  case 'a':
         
      for (int  i = 0; i < 10; i++)
     {  
       for (int  j = 0; j < 20; j++)
      {                          
         array[i][j] = '+';          
           
         
         //else {              
            //array[i][j] = '-';          
           // }    
      }
   }  
   break;

    
    //draw an inverted triangle when var equals b
    //available commands: a, b, c, d, e, f, g, h, i, j, r, t, s
  case 'b':
         
      for (int  i = 0; i < 10; i++)
     {  
       for (int  j = i; j < 20; j++)
      {                          
          array[i][j] = '+';          
           
         
        //else {              
            //array[i][j] = '-';          
           // }   
      }
   }  
   break;
    
    
    //draw a trapeze with pattern when var equals c
    //available commands: a, b, c, d, e, f, g, h, i, j, r, t, s
  case 'c':
         
      for (int  i = 2; i < 7; i++)
     {  
       for (int  j = i; j < 17; j++)
       {                                  
        array[i][j] = '+';  
         
         if (i%2 == 0 && j%3 == 0) 
         {              
			array[i][j] = '-';           
            }      
        }
   }  
   break;
    
    
    
    //draw an inverted triangle with pattern when var equals d
    //available commands: a, b, c, d, e, f, g, h, i, j, r, t, s
  case 'd':
         
      for (int  i = 0; i < 10; i++)
     {  
       for (int  j = i; j < 20; j++)
       {                          
        array[i][j] = '+';         
           if (i%2 == 0 && j%3 == 0) 
           {              
		  	array[i][j] = '-';           
            }   
        }
   }  
   break;
    
    
    
     //draw a trapeze when var equals e
     //available commands: a, b, c, d, e, f, g, h, i, j, r, t, s
  case 'e':
         
       for (int  i = 2; i < 7; i++)
      {  
         for (int  j = i; j < 17; j++)
        {                                  
          array[i][j] = '+';    
          
        //else {              
            //array[i][j] = '-';          
           // }   
          }
   }  
   break;
    
    
    //draw a triangle bottom orientation when var equals f
    //available commands: a, b, c, d, e, f, g, h, i, j, r, t, s
  case 'f':
         
       for (int  i = 1; i < 8; i++)
       {  
         for (int  j = 7; j >= i; j--)
         {                          
           array[i][j] = '+';          
           }
    }  
   break;
    
    
    //draw a triangle top orientation when var equals g
    //available commands: a, b, c, d, e, f, g, h, i, j, r, t, s
  case 'g':
         
       for (int  i = 0; i <= 8; i++)
      {  
         for (int  j = 0; j < i; j++)
        {                          
           array[i][j] = '+';          
           }

    }  
   break;
    
    
    //draw a rectangular triangle  when var equals h
    //available commands: a, b, c, d, e, f, g, h, i, j, r, t, s
  case 'h':
         
       for (int  i = 1; i <= 8; i++)
      {  
         for (int  j = 0; j <=(2 * i - 1); j++)
        {                          
           array[i][j] = '+';          
           }

    }  
   break;
    
        //draw a large triangle with pattern when var equals j
    //available commands: a, b, c, d, e, f, g, h, i, j, r, t, s
  case 'j':
         
       for (int  i = 1; i <= 8; i++)
      {  
         for (int  j = 0; j <=(2 * i - 1); j++)
        {                          
           array[i][j] = '+';
           
           if (i%3 == 0 && j%3 == 0) 
          {              
		    	array[i][j] = '-';           
                }          
           }

    }  
   break;
    
    
    
  default:
    // if nothing else matches, do the default
    // default is optional
    break;
  }   
}


void print(char array[10][20]){
  
  int rows = 10;
  int columns = 20;
  
         for (int  i = 0; i < rows; i++) 
   {  

       for (int  j = 0; j < columns; j++)
   {   
       Serial.print(array[i][j]);
         
       }
           
       Serial.println();
  }
}

// a debugging tool just to get your head around how the cells in our array are numbered
// 0,0 is in the top lefthand corner and 9,19 is in the bottom right
void indices(char array[10][20]){
  
  int rows = 10;
  int columns = 20;
  
         for (int  i = 0; i < rows; i++) 
   {  

       for (int  j = 0; j < columns; j++)
   {   
       Serial.print(i);
       Serial.print(' ');
       Serial.print(j);
       Serial.print(',');         
       }          
       Serial.println();
  }
}
~~~