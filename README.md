## Pre Fab Academy Documentation

#### Hi I'm Selena, this Gitbook is an attempt to document what I'm learning at the Fablab Digiscope, in electronics and programming, as well as digital fabrication.

#### Electronics and programming are new to me as I begin this program, so I try to be accurate and to join links that helped me. To document things is not always an easy process, this is a lot of discoveries so it wasn't easy to choose what to document, and to document "on the go" particularly. Preparing file for documentation is a challenge as well, I have found a series of software very useful to record or prepare file to be published for the web < link to a list of those to come >.

#### "Proto Hut" are researches or little projects I made in parallel with the program, ideas I had for a long time, or things I realised I could do when entering the fablab. 

#### Remember, this is an attempt for documentation, feel free to contact me to get more details or to tell me if things are not clear enough, or even wrong. This would be a valuable feedback for me. Enjoy !






[ci]: https://about.gitlab.com/gitlab-ci/
[GitBook]: https://www.gitbook.com/
[host the book]: https://gitlab.com/pages/gitbook/tree/pages
[install]: http://toolchain.gitbook.com/setup.html
[documentation]: http://toolchain.gitbook.com
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
