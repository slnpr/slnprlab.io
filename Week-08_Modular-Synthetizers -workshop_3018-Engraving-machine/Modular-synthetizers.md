### Modular Synthetizers



#### 1. OSC > FILTER > AMP


![](1-osc-filter-amp.PNG)

Ring oscillator > Summing Amplifier > Low-Pass Filter

![](1-low-pass-filter.PNG)
![](Falstad-1-osc-amp-Low-pass-filter.gif)

https://tinyurl.com/yy8bmrtm


Ring oscillator > Summing Amplifier > High Pass Filter

![](1-high-pass-filter.PNG)
![](Falstad-1-osc-amp-High-pass-filter.gif)

https://tinyurl.com/y5vd8ly9


- Ring Oscillator
"Ring oscillator uses an odd number of inverters to achieve more gain than a single inverting amplifier. The inverter gives a delay to the input signal and if the numbers of inverters are increases then oscillator frequency will be decreased. So the desired oscillator frequency depends on the number of inverter stages of the oscillator.
source : https://www.elprocus.com/ring-oscillator-working-and-its-applications/

- Summing Amplifier
"The Summing Amplifier is another type of operational amplifier circuit configuration that is used to combine the voltages present on two or more inputs into a single output voltage."
source : electronics-tutorials.ws/opamp/opamp_4.html

- Low-Pass Filter
- High-Pass Filter
source : https://www.allaboutcircuits.com/technical-articles/low-pass-filter-tutorial-basics-passive-RC-filter/





#### 2. OSC | OSC > XOR > AMP


![](2-osc-xor-amp.PNG)


Colpitts Oscillator | Ring Oscillator > XOR > Summing Amplifier

![](2-osc-osc-xor-amp.PNG)
![](2-Osc-Osc-Xor-Amp.gif)

https://tinyurl.com/y5ujmlf8

- Colpitts Oscillator
source : https://www.elprocus.com/colpitts-oscillator-circuit-working-and-applications/

- Ring Oscillator

- XOR
The Exclusive-OR gate is a digital logic gate with two inputs and one output. The short form of this gate is Ex-OR. It performs based on the operation of the OR gate. . If any one of the inputs of this gate is high, then the output of the EX-OR gate will be high. The symbol and truth table of the EX-OR are shown below.

![](EXOR-gate-and-its-truth-table.jpg)
source : https://www.elprocus.com/basic-logic-gates-with-truth-tables/


- Summing Amplifier





#### 3. FILTER | SLOW OSC > FREQ > OSC > AMP


![](3-osc_filter-osc-amp.PNG)


![](3-osc-osc-filterr-amp.PNG)
![](3-osc-osc-filterr-amp.gif)


https://tinyurl.com/y4w4mpxy 

https://tinyurl.com/y5sor9ed (High pass filter emplacement modified)

- Phase Shift Oscillator
source : https://www.elprocus.com/rc-phase-shift-oscillator-circuit-using-bjt-frequency/

- Ring Oscillator
- High-Pass Filter
- Summing Amplifier


![](3-osc-osc-memristor-amp.PNG)
![](3.osc-osc-memristor-amp.m4v)

https://tinyurl.com/y7jx9n8m

- Phase Shift Oscillator
- Ring Oscillator
- Memristor

http://www.composelec.com/memristor.php

- Summing Amplifier

![](3-osc-mosfet-amp.PNG)
![](3-ringosc-mosfet-amp.gif)

(with mosfet)
https://tinyurl.com/y8t6ttf4
