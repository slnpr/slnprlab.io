### Harmonograph overview

#### Orignal Machine (2017)


![](Week-12_Programming-pt-II/harmonograph-allpieces.jpg)

![](Week-12_Programming-pt-II/harmonograph-assembling.mp4)

![](Week-12_Programming-pt-II/harmonograph-global.jpg)

![](Week-12_Programming-pt-II/harmonograph-disks.jpg)

![](Week-12_Programming-pt-II/harmonograph-arms.jpg)

![](Week-12_Programming-pt-II/harmonograph-axis-pen.jpg)


#### Arms Axis


![](Week-12_Programming-pt-II/fail-screw.png)

![](Week-12_Programming-pt-II/pen-holder-clip.png)
![](Week-12_Programming-pt-II/pen-holder-clipped.png)

![](Week-12_Programming-pt-II/clip-middle-arm.png)
![](Week-12_Programming-pt-II/clip-middle-arm2.png)

![](Week-12_Programming-pt-II/clip-disk.png)

![](Week-12_Programming-pt-II/harmonograph-v.1.2.mp4)

> Improvement of the "clip-axis" 

![](Week-12_Programming-pt-II/newaxis.png)


#### Motor Holder


![](Week-12_Programming-pt-II/motor-adaptator0.png)

![](Week-12_Programming-pt-II/motor-adaptator1.png)
![](Week-12_Programming-pt-II/motor-adaptator2.png)

![](Week-12_Programming-pt-II/motor-holderv1.png)
![](Week-12_Programming-pt-II/motor-holder-v1.png)

![](Week-12_Programming-pt-II/motor-gearv1.png)

V2

![](Week-12_Programming-pt-II/motorbox1.png)
![](Week-12_Programming-pt-II/motorbox2.png)

![](Week-12_Programming-pt-II/motorbox3.png)
![](Week-12_Programming-pt-II/motorbox-complete.png)

![](Week-12_Programming-pt-II/motor-holder.png)

Attach

![](Week-12_Programming-pt-II/motor-cardboardattach.png)



#### Circuit & Code


![](Week-12_Programming-pt-II/harmonogaph-1motor-circuit.png)

![](Week-12_Programming-pt-II/harmonogaph-2motors-circuit.png)


Code based on Arduino Starter Kit Example : Project 10 - Zoetrope
~~~
/*
  Arduino Starter Kit example
 Project 10  - Zoetrope

 This sketch is written to accompany Project 10 in the
 Arduino Starter Kit

 Parts required:
 two 10 kilohm resistors
 2 momentary pushbuttons
 one 10 kilohm potentiometer
 motor
 9V battery
 H-Bridge

 Created 13 September 2012
 by Scott Fitzgerald
 Thanks to Federico Vanzati for improvements

 http://www.arduino.cc/starterKit

 This example code is part of the public domain
 */

const int controlPin1 = 2; // connected to pin 7 on the H-bridge (sens2 moteur1)
const int controlPin4 = 12; // connected to pin 15 on the H-bridge (sens2 moteur2)
const int controlPin2 = 3; // connected to pin 2 on the H-bridge (sens 1 moteur1)
const int controlPin3 = 11; // connected to pin 10 on the H-bridge (sens 1 moteur2)
const int enablePin = 9;   // connected to pin 1 on the H-bridge  (vitesse moteur 1)
const int enablePin2 = 10;   // connected to pin 9 on the H-bridge  (vitesse moteur 2)
const int directionSwitchPin = 4;  // connected to the switch for direction (bouton direction)
const int directionSwitchPin2 = 7;  // connected to the switch for direction (bouton direction2)
const int onOffSwitchStateSwitchPin = 5; // connected to the switch for turning the motor on and off
const int onOffSwitchStateSwitchPin2 = 6; // connected to the switch2 for turning the motor on and off
const int potPin = A0;  // connected to the potentiometer's output
const int potPin2 = A1;  // connected to the 2nd potentiometer's output

// create some variables to hold values from your inputs
int onOffSwitchState = 0;  // current state of the On/Off switch
int previousOnOffSwitchState = 0; // previous position of the on/off switch
int directionSwitchState = 0;  // current state of the direction switch
int previousDirectionSwitchState = 0;  // previous state of the direction switch

int motorEnabled = 0; // Turns the motor on/off
int motorSpeed = 0; // speed of the motor
int motorDirection = 1; // current direction of the motor

int onOffSwitchState2 = 0;  // current state of the On/Off switch
int previousOnOffSwitchState2 = 0; // previous position of the on/off switch
int directionSwitchState2 = 0;  // current state of the direction switch
int previousDirectionSwitchState2 = 0;  // previous state of the direction switch

int motorEnabled2 = 0; // Turns the motor on/off
int motorSpeed2 = 0; // speed of the motor
int motorDirection2 = 1; // current direction of the motor

void setup() {
  // intialize the inputs and outputs
  pinMode(directionSwitchPin, INPUT);
  pinMode(onOffSwitchStateSwitchPin, INPUT);
  pinMode(controlPin1, OUTPUT);
  pinMode(controlPin2, OUTPUT);
  pinMode(enablePin, OUTPUT);
  pinMode(directionSwitchPin2, INPUT);
  pinMode(onOffSwitchStateSwitchPin2, INPUT);
  pinMode(controlPin4, OUTPUT);
  pinMode(controlPin3, OUTPUT);
  pinMode(enablePin2, OUTPUT);

  // pull the enable pin LOW to start
  digitalWrite(enablePin, LOW);
  digitalWrite(enablePin2, LOW);
}

void loop() {
  // read the value of the on/off switch
  onOffSwitchState = digitalRead(onOffSwitchStateSwitchPin);
  onOffSwitchState2 = digitalRead(onOffSwitchStateSwitchPin2);
  delay(1);

  // read the value of the direction switch
  directionSwitchState = digitalRead(directionSwitchPin);
  directionSwitchState2 = digitalRead(directionSwitchPin2);

  // read the value of the pot and divide by 4 to get
  // a value that can be used for PWM
  motorSpeed = analogRead(potPin) / 4;
  motorSpeed2 = analogRead(potPin2) / 4;

  // if the on/off button changed state since the last loop()
  if (onOffSwitchState != previousOnOffSwitchState) {
    // change the value of motorEnabled if pressed
    if (onOffSwitchState == HIGH) {
      motorEnabled = !motorEnabled;
    }
  }
  if (onOffSwitchState2 != previousOnOffSwitchState2) {
    // change the value of motorEnabled if pressed
    if (onOffSwitchState2 == HIGH) {
      motorEnabled2 = !motorEnabled2;
    }
  }

  // if the direction button changed state since the last loop()
  if (directionSwitchState != previousDirectionSwitchState) {
    // change the value of motorDirection if pressed
    if (directionSwitchState == HIGH) {
      motorDirection = !motorDirection;
    }
  }
  if (directionSwitchState2 != previousDirectionSwitchState2) {
    // change the value of motorDirection if pressed
    if (directionSwitchState2 == HIGH) {
      motorDirection2 = !motorDirection2;
    }
  }

  // change the direction the motor spins by talking
  // to the control pins on the H-Bridge
  if (motorDirection == 1) {
    digitalWrite(controlPin1, HIGH);
    digitalWrite(controlPin2, LOW);
  } else {
    digitalWrite(controlPin1, LOW);
    digitalWrite(controlPin2, HIGH);
  }
  if (motorDirection2 == 1) {
    digitalWrite(controlPin4, HIGH);
    digitalWrite(controlPin3, LOW);
  } else {
    digitalWrite(controlPin4, LOW);
    digitalWrite(controlPin3, HIGH);
  }

  // if the motor is supposed to be on
  if (motorEnabled == 1) {
    // PWM the enable pin to vary the speed
    analogWrite(enablePin, motorSpeed);
  } else { // if the motor is not supposed to be on
    //turn the motor off
    analogWrite(enablePin, 0);
  }
  if (motorEnabled2 == 1) {
    // PWM the enable pin to vary the speed
    analogWrite(enablePin2, motorSpeed2);
  } else { // if the motor is not supposed to be on
    //turn the motor off
    analogWrite(enablePin2, 0);
  }
  // save the current On/Offswitch state as the previous
  previousDirectionSwitchState = directionSwitchState;
  previousDirectionSwitchState2 = directionSwitchState2;
  // save the current switch state as the previous
  previousOnOffSwitchState = onOffSwitchState;
  previousOnOffSwitchState2 = onOffSwitchState2;
}

~~~


#### It's turning, but... it's powerless


![](Week-12_Programming-pt-II/harmonographe-motor-test.mp4)

![](Week-12_Programming-pt-II/harmonographe_powerless.mp4)



#### Servo Motor Holder


As the motors were not enough, I was adviced by Jonah to try with Continuous Servo Motors, using this tutorial (https://learn.adafruit.com/modifying-servos-for-continuous-rotation/overview) explaining how to make a continuous servomotor out of a 180 degrees servomotor. 
Forunately, I had continuous servomotor. 
Modifications of the box holding the dc motors had to be made in order to hold a servomotor instead.

![](Week-12_Programming-pt-II/servomotoradaptor-fusion360.png)
![](Week-12_Programming-pt-II/servomotor-adaptator.png)

![](Week-12_Programming-pt-II/Servomotor-holder.gif)

The very simple circuit using 2 continuous servomotors

![](Week-12_Programming-pt-II/harmonographe-circuit-2servomotor.png)

The circuit on : 

![](Week-12_Programming-pt-II/servomotor-on.gif)

The Arduino sketch :

~~~
#include <Servo.h>
Servo myservo;
Servo myservo2;

void setup() {
  myservo.attach(9); 
  myservo2.attach(8); 
}

void loop() {
  myservo.write(0); //rotate counterclockwise at slow speed
  myservo2.write(0);
}
~~~

> Note : When unsing a continuous servomotor "myservo.write()" use 0 to rotate the servomotor counterclokwise at slow speed, 180 to rotate the servomote clokwise at slow speed, and 90 to hold the servo in neutral position. The values between 0 and 90, and 0 and 180, allows to control the speed of rotation.

#### Harmonograph motorized by two continuous servomotors

![](Week-12_Programming-pt-II/harmonographe-servomotor2.mp4)
