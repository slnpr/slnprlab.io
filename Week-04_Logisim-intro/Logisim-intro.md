### Introduction to Logisim

Logisim is an educationnal and open source program for designing and simulating digital logic circuits.

Here is Logisim's website : http://www.cburch.com/logisim/

This workshop is introducing basic logic circuit to reproduce with Logisim.

#### 0. Logic gates and their truth table

![](Week-04_Logisim-intro/TP-1-logisim-intro-Logic-gates.png)

#### 1. Half-adder circuit

![](Week-04_Logisim-intro/TP1-Logisim-intro_1-Demi-additionneur.gif)

#### 2. Complete-adder circuit

![](Week-04_Logisim-intro/TP1-Logisim-intro_2-Additionneur-complet.gif)

#### 3. Light wheel

![](Week-04_Logisim-intro/TP1-Logisim-intro_3-Roue-lumineuse.gif)

#### 4. Traffic light

![](Week-04_Logisim-intro/TP-1-logisim-intro-4.png)

![](Week-04_Logisim-intro/TP1-Logisim-intro_4-Feux-circulation.gif)

#### 5. Improved traffic light

![](Week-04_Logisim-intro/TP-1-logisim-intro-5.png)

![](Week-04_Logisim-intro/TP1-Logisim-intro_5-Feux-circulation.gif)
